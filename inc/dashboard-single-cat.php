<div class="jkrcat_outer">
<h3><?php _e('Animal Admit Form v1.0','jkrcat'); ?></h3>
<?php 
if(isset($_GET['cats_id']) && $_GET['cats_id']>0){
    $post = get_post($_GET['cats_id']);
    // echo '<pre>';
    // print_r($post);
    // echo '</pre>';
    setup_postdata($post);
        ?>
            <h1>Animal: <?=$post->post_title;?></h1>
            <div class="jkrcat_block small">Basic Information <a href="<?php echo get_edit_post_link($post->ID); ?>" class="btn btn-success">Edit Animal</a> <a href="<?php the_permalink($post->ID); ?>" target="_blank" class="btn btn-primary">View Frontend</a></div>
            <div class=" small">
                <div class="list_items">
                    <div class="item" style="background-color: #fff;">
                        <div class="col-sm-6">
                            <label class="red">AARU CODE</label>
                            <span class="style_input"><?=get_field('aaru_code', $post->ID);?></span>
                        </div>
                        <div class="col-sm-6">
                            <?php 
                                echo '<label style="width:220px;"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-cat.png" alt=""> CAT NAME</label>';
                            ?>
                            <span class="style_input"><?=$post->post_title;?></span>
                        </div>
                    </div>
                </div>
            </div> <!-- /.jkrcat_block  -->
            <div class="jkrcat_block">
                <div class="row">
                    <div class="col-sm-8 table-responsive">
                        <table class="table">
                            <tr>
                                <td>SCAN & IF CHIP FOUND CONTACT OWNER</td>
                                <td><span class="style_input"><?=get_field('scan_&_if_chip_found_contact_owner', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>CHIP NUMBER IF IMPLANTED WHEN NEUTERED</td>
                                <td><span class="style_input"><?=get_field('chip_number_if_implanted_when_neutered', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>CHIPPED</td>
                                <td><span class="style_input"><?=get_field('chipped', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>Data</td>
                                <td><span class="style_input"><?=get_field('data', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>SEX</td>
                                <td><span class="style_input"><?=get_field('gender', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>FLEAED</td>
                                <td><span class="style_input"><?=get_field('fleaed', $post->ID);?></span></td>
                            </tr>
                            
                            <tr>
                                <td>BLOODS</td>
                                <td>
                                    <?php 
                                        $bloods = get_field('bloods', $post->ID);
                                        if($bloods){
                                            foreach($bloods as $b){
                                                echo '<span class="style_input">'.$b.'</span>';
                                            }
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>WEIGHT (KG)</td>
                                <td><span class="style_input"><?=get_field('weight_kg', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td width="47%">VACCINATIONS<br><span class="red">IF NO FELV STATE HEALTH ISSUE</span></td>
                                <td><span class="style_input"><?=get_field('vaccinations', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="padding:0">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <span>1st Data &nbsp; &nbsp; &nbsp;</span><br>
                                                <span class="style_input"><?=get_field('vacc_data', $post->ID);?></span>
                                            </td>
                                            <td>
                                                <span>2nd Data &nbsp; &nbsp; &nbsp;</span><br>
                                                <span class="style_input"><?=get_field('vacc_data2', $post->ID);?></span>
                                            </td>
                                            <td>
                                                <span>Booster &nbsp; &nbsp; &nbsp;</span><br>
                                                <span class="style_input"><?=get_field('vacc_data3', $post->ID);?></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="47%">FFC Collar</td>
                                <td><span class="style_input"><?=get_field('ffc_collar', $post->ID);?></span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4  table-responsive">
                        <table class="table">
                            <tr>
                                <td>NEUTERED</td>
                                <td><span class="style_input"><?=get_field('neutered', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>AGE</td>
                                <td><span class="style_input"><?=get_field('age', $post->ID);?></span></td>
                            </tr>
                            <tr>
                                <td>WORMED</td>
                                <td><span class="style_input"><?=get_field('wormed', $post->ID);?></span></td>
                            </tr>
                            
                        </table>
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.jkrcat_block -->

            <div class="jkrcat_block">
                <div>
                    COMMENTS<br>
                    <span class="style_input"><?=get_field('comments', $post->ID);?></span>
                </div>
                <p>&nbsp;</p>
                <div class="row">
                    <div class="col-md-8 table-responsive">
                        <table class="table">
                            <tr>
                                <td width="47%">DESCRIPTION OF ANIMAL <span class="red">FE=CAT  OT=OTHER</span></td>
                                <td><span class="style_input"><?=get_field('description_of_animal', $post->ID);?></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div> <!-- /.jkrcat_block -->
            
            <div>
                <p>&nbsp;</p>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="jkrcat_block small">FOSTERER DETAILS</div>
                    <div class="jkrcat_block">
                        ASSIGNED FOSTERER:
                        <?php if(get_field('fosterer', $post->ID)){
                            $fosterer_id = get_field('fosterer', $post->ID);
                            global $wpdb;
                            $wpdb->jkrcat_fostere = $wpdb->prefix."jkrcat_fostere";
                            $sql = "SELECT fostere_id,name FROM $wpdb->jkrcat_fostere WHERE fostere_id = $fosterer_id";
                            $fosterer = $wpdb->get_row($sql);

                            $animals = $this->ac_count_animal_count_for_fostere($fosterer_id);
                            echo '<div class="list_items"><div class="item">
                                <div class="col-sm-4">
                                    <label>Name</label>
                                    <span>'.$fosterer->name.'</span>
                                </div>

                                <div class="col-sm-4">
                                    <label>Animals</label>
                                    <span>'.count($animals).'</span>
                                </div>
                                <div class="meta col-sm-4 justify-content-end">
                                    <a class="view_fostere" href="'.$fosterer->fostere_id.'"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-view.png" alt=""> View</a>
                                    ';
                                    if(current_user_can('administrator')){
                                        echo '<a class="red delete_fostere_cat" href="'.$post->ID.'" data-fosterid="'.$fosterer->fostere_id.'">Delete</a> <span class="msg">Wait</span>';
                                    }
                                echo '</div>
                            </div></div>';
                        }else{ // no fostere assigned 
                            echo '<span class="red">This Animal not assigned to any Fostere</span>';
                        } ?>
                    </div>

                    <div class="jkrcat_block">
                      
                        <div> <!-- Existing fosterer add -->
                            <span class="red">LINK THIS ANIMAL TO CURRENT FOSTERER?</span><br>
                            <p>Select exisiting Fosterer: </p>
                            <?php 
                            global $wpdb;
                            $wpdb->jkrcat_fostere = $wpdb->prefix."jkrcat_fostere";
                            $sql = "SELECT fostere_id,name,mobile FROM $wpdb->jkrcat_fostere ORDER BY name";
                            $result = $wpdb->get_results($sql);
                            echo '<div class="field-set"><select id="existing_fosterer" data-catid="'.$post->ID.'">';
                                echo '<option value="0">No Fosterer</option>';
                            foreach($result as $item){
                                echo '<option value="'.$item->fostere_id.'" '.(($item->fostere_id == $fosterer_id)?'selected="selected"': '').'>'.$item->name.'</option>';
                            }
                            echo '</select></div><div class="existing_fosterer_msg"></div>';
                            ?>

                        </div> <!-- /Existing fosterer add -->
                        <p>&nbsp;</p> 
                        <hr>
                           
                        <p>Add a new Fosterer</p>    
                        <div class="page_msg"></div> <!-- jquery -->
                        <form  id="form_add_fostere">
                            <div class="row">
                            <div class="col-sm-8">
                                <div class="field-set">
                                    <label>NAME</label>
                                    <input type="text" name="name" placeholder="Fostere Name" required >
                                    <input type="hidden" name="cats_id" value="<?=$post->ID?>" required>
                                </div> <!-- /field set -->

                                <div class="field-set">
                                    <label>EMAIL</label>
                                    <input type="text" name="email" placeholder="Email" >
                                </div> <!-- /field set -->

                                <div class="field-set">
                                    <label>ADDRESS</label>
                                    <input type="text" name="address" placeholder="Address" required >
                                </div> <!-- /field set -->
                                
                                <div class="field-set">
                                    <label>LANDLINE</label>
                                    <input type="text" name="phone" placeholder="Landline">
                                </div> <!-- /field set -->

                                <div class="field-set">
                                    <label>MOBILE</label>
                                    <input type="text" name="mobile" placeholder="Mobile" required >
                                </div> <!-- /field set -->
                            </div> <!-- col-sm-6 -->
                            <div class="col-sm-4">
                                <div class="image-field">
                                    <img id="background_image_show" src="<?php echo plugins_url('../assets/images/congress-no-image.jpg', __FILE__); ?>" alt="User image" data-old="<?=plugins_url('../assets/images/congress-no-image.jpg', __FILE__)?>"><br>
                                    
                                    <input id="background_image" type="hidden" name="background_image" />
                                    <input id="background_image_url" type="hidden" name="background_image_url" /> <!-- save url on page refrash -->
                                    
                                    <input id="upload_image_button" type="button" class="btn btn-primary" value="Insert Image" /><br>

                                    <input id="upload_image_remove" type="button" class="btn"  disabled value="Remove Image" />
                                </div>
                            </div> <!-- col-sm-6 -->
                            <div class="col-sm-12">
                                <p> </p>
                                <div class="field-set right">
                                    <input type="submit" id="submit" value="+ Add Volunteers/Fosterers">
                                </div> <!-- /field set -->
                            </div> <!-- wd70 -->
                            </div> <!-- .row -->
                        </form>
                            
                      
                    </div> <!-- /.jkrcat_block -->
                </div> <!-- /left side, FOSTERER DETAILS -->
                <div class="col-md-6">
                    <div class="jkrcat_block small">ADOPTOR DETAILS</div>
                    <div class="jkrcat_block">
                        FOSTERER:
                    </div>
                </div> <!-- /right side, FOSTERER DETAILS -->
            </div> <!-- / Fosterer / Adoptor list -->


        <?php
    wp_reset_postdata();
}else{
    echo '<h1>Animal: Unkown </h1>';
    echo '<h4 class="red">No data found, please try again.</h4>';
}
?>

</div> <!-- /plugin outer -->