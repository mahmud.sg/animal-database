<div class="jkrcat_outer">
<?php 
    // all forsteres
    global $wpdb;
    $wpdb->jkrcat_fostere = $wpdb->prefix."jkrcat_fostere";
    $sql = "SELECT fostere_id,name,mobile FROM $wpdb->jkrcat_fostere";

    $total_query     = "SELECT COUNT(1) FROM $wpdb->jkrcat_fostere";
    $total             = $wpdb->get_var( $total_query );
    $items_per_page = 20;
    $page             = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
    $offset         = ( $page * $items_per_page ) - $items_per_page;
    $result         = $wpdb->get_results( $sql . " ORDER BY fostere_id DESC LIMIT ${offset}, ${items_per_page}" );
    $totalPage         = ceil($total / $items_per_page);

    
?>

<h3><?php _e('Animal Admit Form v1.0','jkrcat'); ?></h3>
<h1>Fosteres list</h1>
<!-- <div class="jkrcat_block small">
    <h3 class="foster_name">All Fosteres</h3>
</div> -->

<div class="jkrcat_block">
    <div class="page_msg"></div> <!-- by js -->
    <div class="list_items">
        <?php 
         if($total && $result ) {
            foreach($result as $item){
                $animals = $this->ac_count_animal_count_for_fostere($item->fostere_id);

                echo '<div class="item row">
                    <div class="col-sm-4">
                        <label>Name</label>
                        <span>'.$item->name.'</span>
                    </div>
                    <div class="col-sm-3">
                        <label>Mobile</label>
                        <span><a href="tel:'.$item->mobile.'">'.$item->mobile.'</a></span>
                    </div>
                    <div class="col-sm-2">
                        <label>Animals</label>
                        <span>'.count($animals).'</span>
                    </div>
                    <div class="meta col-sm-3 justify-content-end">
                        <a class="view_fostere" href="'.$item->fostere_id.'"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-view.png" alt=""> View</a>
                        <a class="mail_fostere" href="'.$item->fostere_id.'"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-mail.png" alt=""> Email</a>';
                        if(current_user_can('administrator')){
                            echo '<a class="red delete_fostere" href="'.$item->fostere_id.'">Delete</a> <span class="msg">Wait</span>';
                        }
                    echo '</div>
                </div>';
            }
        }else{
            echo '<h4 class="red">No more Foster found</h4>';
        }
        ?>
    </div>
    <div class="jk_pagination">
        <?php 
        if($totalPage > 1){
            $customPagHTML     =  '<div><span>Page '.$page.' of '.$totalPage.'</span>'.paginate_links( array(
            'base' => add_query_arg( 'cpage', '%#%' ),
            'format' => '',
            'prev_text' => __('&laquo;'),
            'next_text' => __('&raquo;'),
            'total' => $totalPage,
            'current' => $page
            )).'</div>';

            echo $customPagHTML;
        }
        ?>
    </div>
</div>

<?php $this->jk_plugin_footer_html(); ?>

    </div> <!-- /plugin outer -->