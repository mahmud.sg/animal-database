<?php 
if(isset($_GET['fos']) && $_GET['fos']){
    $user_id = $_GET['fos'];
    global $wpdb;
    $table = $wpdb->prefix."jkrcat_fostere";

    $user_details = $wpdb->get_row("SELECT * FROM $table WHERE fostere_id = $user_id LIMIT 1");
    if($user_details && $user_details->photo > 0){
        $old_user_photo = wp_get_attachment_image_src($user_details->photo, 'medium');
    }else{
        $old_user_photo = NULL;
    }
}else{
    $user_details = $user_id = $old_user_photo = NULL;
    // $user_id = NULL;
}
?>
<div class="jkrcat_outer">
    <h3><?php _e('Animal Admit Form v1.0','jkrcat'); ?></h3>
    <h1> <?php echo ($user_id != NULL)? 'Edit Fostere' :'Add Fostere'; ?> </h1>
    
    <?php if($user_id && $user_details){?>
        <div class="jkrcat_block small">
            <h3 class="foster_name"><a href="<?php echo add_query_arg('page', 'jkrcat_fosteres_list'); ?>&fos=<?=$user_id?>"><?=$user_details->name?></a></h3>
        </div>
    <?php } // if edit ?>

    <?php if($user_id && !$user_details){?>
        <div class="jkrcat_block">
            <h3 class="red">Sorry! User data not found, please try again.</h3>
        </div>
    <?php }else{ ?>
        <div class="jkrcat_block">
            <div class="page_msg"></div> <!-- jquery -->
            <form  id="form_add_fostere">
                <div class="row">
                <div class="col-sm-6">
                    <div class="field-set">
                        <label>NAME</label>
                        <input type="text" name="name" placeholder="Fostere Name" required value="<?php echo ($user_details)? $user_details->name : ''; ?>">
                        <?php if($user_details){
                            echo '<input type="hidden" id="user_id" name="user_id" value="'.$user_details->fostere_id.'" required>';
                        } ?>
                    </div> <!-- /field set -->

                    <div class="field-set">
                        <label>EMAIL</label>
                        <input type="text" name="email" placeholder="Email" value="<?php echo ($user_details)? $user_details->email : ''; ?>">
                    </div> <!-- /field set -->

                    <div class="field-set">
                        <label>ADDRESS</label>
                        <input type="text" name="address" placeholder="Address" required value="<?php echo ($user_details)? $user_details->address : ''; ?>">
                    </div> <!-- /field set -->
                    
                    <div class="field-set">
                        <label>LANDLINE</label>
                        <input type="text" name="phone" placeholder="Landline" value="<?php echo ($user_details)? $user_details->phone : ''; ?>">
                    </div> <!-- /field set -->

                    <div class="field-set">
                        <label>MOBILE</label>
                        <input type="text" name="mobile" placeholder="Mobile" required value="<?php echo ($user_details)? $user_details->mobile : ''; ?>">
                    </div> <!-- /field set -->
                </div> <!-- col-sm-6 -->
                <div class="col-sm-4">
                    <div class="image-field">
                        <img id="background_image_show" src="<?php echo ($old_user_photo)? $old_user_photo[0] : plugins_url('../assets/images/congress-no-image.jpg', __FILE__); ?>" alt="User image" data-old="<?=plugins_url('../assets/images/congress-no-image.jpg', __FILE__)?>"><br>
                        
                        <input id="background_image" type="hidden" name="background_image" value="<?php echo ($old_user_photo)? $user_details->photo : ''; ?>" />
                        <input id="background_image_url" type="hidden" name="background_image_url" value="<?php echo ($old_user_photo)? $old_user_photo[0] : ''; ?>" /> <!-- save url on page refrash -->
                        
                        <input id="upload_image_button" type="button" class="btn btn-primary" value="Insert Image" /><br>

                        <?php if($old_user_photo){ // if edit and has photo ?>
                            <input id="upload_image_remove" type="button" class="btn"  disabled value="Remove Image" />
                        <?php }else{ ?>
                            <input id="upload_image_remove" type="button" class="btn" value="Remove Image" />
                        <?php } ?>
                        
                    </div>
                </div> <!-- col-sm-6 -->
                <div class="col-sm-6">
                    <div class="field-set right">
                        <?php if($user_id){?>
                            <a class="btn btn-secondary" href="<?php echo add_query_arg('page', 'jkrcat_fosteres_list'); ?>&fos=<?=$user_id?>">View Fostere</a> &nbsp;
                        <?php } ?>
                        <input type="submit" id="submit" value="<?php echo ($user_details)? 'Update Fostere' : 'Add Fostere'; ?>">
                    </div> <!-- /field set -->
                </div> <!-- wd70 -->
                </div> <!-- .row -->
            </form>

            <!-- <hr>
            <div class="current_assigned_animals">
                <h3>Current Assigned Animals</h3>
                <div class="items cates">
                    xxxx
                </div>
            </div> -->
        </div>
    <?php } // if edit and edit data not found  ?>
</div> <!-- /.jkrcat_outer -->