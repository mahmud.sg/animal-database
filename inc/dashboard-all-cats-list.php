
<div class="jkrcat_outer">
<?php 
    // ========= all Animals 
    $items_per_page = 10;
    $page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;
    $offset         = ( $page * $items_per_page ) - $items_per_page;

    $args = array(
        'posts_per_page'   => $items_per_page,
        'offset' => (int)(10 * ($page - 1)),
        'post_type'     => 'animal',
        'post_status'   => array('publish', 'pending', 'draft', 'future', 'private', 'inherit'),
    );
    $result = get_posts($args);

    // find out total 
    $args1 = array(
        'posts_per_page'   => -1,
        'post_type'     => 'animal',
        'post_status'   => array('publish', 'pending', 'draft', 'future', 'private', 'inherit'),
    );
    
    $total = count(get_posts($args1));
    $totalPage         = ceil($total / $items_per_page);
?>

<h3><?php _e('Animal Admit Form v1.0','jkrcat'); ?></h3>
<h1>Current Animals</h1>
<!-- <div class="jkrcat_block small">
    <h3 class="foster_name">All Fosteres</h3>
</div> -->

<div class="jkrcat_block">
    <div class="page_msg"></div> <!-- by js -->
    <div class="list_items">
        <?php 
         if($total && $result ) {
            global $wpdb;
            $foster_table = $wpdb->prefix."jkrcat_fostere";

            foreach($result as $item){
                // Foster name 
                $foster_id = get_field('fosterer', $item->ID);
                if($foster_id){
                    $fostere = $wpdb->get_row("SELECT fostere_id,name FROM $foster_table WHERE fostere_id = $foster_id");
                    $foster_output = '<a class="view_fostere" href="'.$fostere->fostere_id.'">'.$fostere->name.'</a>';
                }else{ 
                    $foster_output = '<span class="red">NONE</span>';
                } // end forstere name

                echo '<div class="item row">
                    <div class="col-md-3">
                        <label class="red">AARU CODE</label>
                        <span>'.(get_field('aaru_code', $item->ID)? get_field('aaru_code', $item->ID) : '<span class="red">NONE</span>').'</span>
                    </div>
                    <div class="col-md-3">
                        <label style="width:220px;"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-cat.png" alt=""> CAT NAME</label>
                        <span>'.$item->post_title.'</span>
                    </div>
                    <div class="col-md-3">
                        <label>FOSTERER</label>
                        <span>'.$foster_output.'</span>
                    </div>
                    <div class="meta col-md-3 justify-content-end">
                        <a class="view_cats" href="'.add_query_arg('page', 'jkrcat_setting').'&cats_id='.$item->ID.'"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-view.png" alt=""> View</a>
                        <a class="mail_cats" href="'.add_query_arg('page', 'jkrcat_setting').'&email_cat='.$item->ID.'"><img src="'.plugin_dir_url( __FILE__ ).'../assets/images/cat-mail.png" alt=""> Email</a>';
                        if(current_user_can('administrator')){
                            echo '<a class="red " href="'.get_edit_post_link($item->ID).'">Delete</a> <span class="msg">Wait</span>';
                        }
                    echo '</div>
                </div>';
            }
        }else{
            echo '<h4 class="red">No more Cats found.</h4>';
        }
        ?>
    </div>
    <div class="jk_pagination">
        <?php 
        if($totalPage > 1){
            $customPagHTML     =  '<div><span>Page '.$page.' of '.$totalPage.'</span>'.paginate_links( array(
            'base' => add_query_arg( 'cpage', '%#%' ),
            'format' => '',
            'prev_text' => __('&laquo;'),
            'next_text' => __('&raquo;'),
            'total' => $totalPage,
            'current' => $page
            )).'</div>';

            echo $customPagHTML;
        }
        ?>
    </div>
</div>
</div> <!-- /plugin outer -->