<?php 
global $wpdb;
$charset_collate = $wpdb->get_charset_collate();
$table_name = $wpdb->prefix . "jkrcat_fostere"; 
$table_name2 = $wpdb->prefix . "jkrcat_adoptor"; 
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

#Check to see if the table exists already, if not, then create it
if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $table_name){
    $sql = "CREATE TABLE $table_name ( 
        fostere_id MEDIUMINT(9) NOT NULL AUTO_INCREMENT , 
        `name` varchar(100) NOT NULL,
        `address` varchar(200) NOT NULL,
        `email` varchar(100) NOT NULL,
        `mobile` varchar(20) NOT NULL,
        `phone` varchar(20) NOT NULL,
        `photo` varchar(255) NOT NULL,
        `create-data` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        `modify-data` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY (fostere_id),
        UNIQUE KEY `mobile` (`mobile`)
    ) $charset_collate;";
    dbDelta( $sql );
}

#Check to see if the table exists already, if not, then create it
if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $table_name2){
    $sql = "CREATE TABLE $table_name ( 
        adoptor_id MEDIUMINT(9) NOT NULL AUTO_INCREMENT , 
        `name` varchar(100) NOT NULL,
        `address` varchar(200) NOT NULL,
        `email` varchar(100) NOT NULL,
        `mobile` varchar(20) NOT NULL,
        `phone` varchar(20) NOT NULL,
        `photo` varchar(255) NOT NULL,
        `create-data` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        `modify-data` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY (adoptor_id),
        UNIQUE KEY `mobile` (`mobile`)
    ) $charset_collate;";
    // require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
?>