
<div class="jkrcat_outer">
<?php 
    // ========= Mail cat
    global $wpdb;
    if(isset($_GET['email_cat']) && $_GET['email_cat']>0){
        $cat_id = (int)$_GET['email_cat'];
        
        $cat = get_post($cat_id); // get cat data

        $foster_table = $wpdb->prefix."jkrcat_fostere";
        $foster_id = get_field('fosterer', $cat->ID); // get fosterer
        if($foster_id){
            $fostere = $wpdb->get_row("SELECT fostere_id,name FROM $foster_table WHERE fostere_id = $foster_id");
            $foster_output = '<a class="view_fostere" href="'.$fostere->fostere_id.'">'.$fostere->name.'</a>';
        }else{ 
            $foster_output = '<span class="red">NONE</span>';
        } // end forstere name

    }else{
        $cat_id = NULL;
    }
    
?>

<h3><?php _e('Animal Admit Form v1.0','jkrcat'); ?></h3>
<h1>Email Animal Details</h1>
<div class="jkrcat_block small">
    <h3>Send Animal details via email </h3>
</div>

<div class="jkrcat_block">
    <p class="red">Please select an Animals profile to email </p>
    <div class="mail_select_animal">
        
        <?php 
            if($cat_id){
                echo '<div class="list_items">
                    <div class="item row">
                        <div class="col-sm-4">
                            <label class="red">AARU CODE</label>
                            <span>'.(get_field('aaru_code', $cat->ID)? get_field('aaru_code', $cat->ID) : '<span class="red">NONE</span>').'</span>
                        </div>
                        <div class="col-sm-4">
                            <label>CAT NAME</label>
                            <span>'.$cat->post_title.'</span>
                        </div>
                        <div class="col-sm-4">
                            <label>FOSTERER:</label>
                            <span>'.$foster_output.'</span>
                        </div>
                    </div></div>';
            }else{
                echo '<p>You haven\'t select any Animal, please select one.</p>';
            }
        ?>
        
        <div class="right">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <?php 
                    $args = array(
                        'posts_per_page'   => -1,
                        'post_type'     => 'animal',
                        'post_status'   => array('publish', 'pending', 'draft', 'future', 'private', 'inherit'),
                    );
                    $cats = get_posts($args);
                    if($cats){
                        foreach($cats as $cat){
                            echo '<a class="dropdown-item" href="'.add_query_arg('page', 'jkrcat_setting').'&email_cat='.$cat->ID.'">AARU-CODE: <strong>'.get_field('aaru_code', $cat->ID).'</strong> , NAME: <strong>'.$cat->post_title.'</strong></a>';
                        }
                    }else{
                        echo '<a class="dropdown-item" href="#">No Animals found!</a>';
                    }
                    ?>
                </div>
            </div>
        </div> <!-- /.right -->
    </div> <!-- /.mail_select_animal -->
    
        <div class="email_body">
            <p>&nbsp;</p>
            <div class="page_msg"></div>
            <form id="form_email_send" method="post">
                <input type="hidden" name="cat_id" required value="<?=$cat_id?>">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td>To</td>
                            <td><input type="text" name="mail_to" required></td>
                        </tr>
                        <tr>
                            <td>BCC</td>
                            <td><input type="text" name="mail_bcc" required></td>
                        </tr>
                        <tr>
                            <td>SUBJECT</td>
                            <td><input type="text" name="subject" required></td>
                        </tr>
                        <tr>
                            <td>MESSAGE</td>
                            <td><textarea name="message" required cols="30" rows="7">Hi!
Please see the details for  Sherlock attached.
--------------
Thanks
Robinhood Cat Rescue 
                            </textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="submit" value="Send Email"></td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
            
        </div>
    </div>
</div>