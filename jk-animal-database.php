<?php
/*
Plugin Name: Robin Hood Cats Animal Database
Plugin URI: https://www.infusedmedia.co.uk/
Description: Animal databse, to keep all info of Animals, Fosteres, Adoptor and create report and send mail to Adoptor
Author: Mahmud
Version: 1.0
Author URI: https://jakariaalmahmud.com
Text Domain: jkrcat
*/
if(!defined('JKRCAT_PLG_DIR')){
    define('JKRCAT_PLG_DIR', plugin_basename(dirname(__FILE__)));
}
if(!class_exists('jk_robin_hood_cat')){
    class jk_robin_hood_cat
    {
        public function __construct(){
            register_activation_hook(__FILE__, array(&$this, 'jkrcat_install'));
            add_action('admin_menu', array(&$this, 'jkrcat_add_options_pages'));
            add_action('admin_enqueue_scripts', array(&$this, 'jkrcat_add_assets'));

            add_action('admin_enqueue_scripts', array(&$this, 'media_uploader_enqueue')); // use default wp media uploader

            add_action('wp_ajax_ac_delete_fostere', array($this,'ac_delete_fostere_ajax_callback')); // fostere-list
            add_action('wp_ajax_ac_add_fostere', array($this,'ac_add_fostere_ajax_callback')); // fostere-create
            add_action('wp_ajax_ac_view_fostere', array($this,'ac_view_fostere_ajax_callback')); // fostere-view
            add_action('wp_ajax_ac_add_existing_fosterer_cat', array($this,'ac_add_existing_fosterer_cat_ajax_callback')); // existing foster to cat
            add_action('wp_ajax_ac_delete_foster_from_cat', array($this,'ac_delete_foster_from_cat_ajax_callback')); // remove fosterer from cat

        }

        /* 
        install plugin 
        @crate tables
        */
        public function jkrcat_install()
        {
            include 'inc/database.php';
        }

        /* 
        Add menu on admin
        */
        public function jkrcat_add_options_pages(){
            add_submenu_page('edit.php?post_type=animal', __('Animal Admit Form v1.0','jkrcat'), __('Animal Database','jkrcat'), 'manage_options', 'jkrcat_setting', array($this, 'jkrcat_options_page'));
            add_submenu_page('edit.php?post_type=animal', __('Fosteres List | Animal Admit Form v1.0','jkrcat'), __('Fosteres List','jkrcat'), 'manage_options', 'jkrcat_fosteres_list', array($this, 'jkrcat_fosteres_list_page'));
            add_submenu_page('edit.php?post_type=animal', __('Create Fostere | Animal Admit Form v1.0','jkrcat'), __('Create Fostere','jkrcat'), 'manage_options', 'jkrcat_fostere_add', array($this, 'jkrcat_fostere_create_page'));
            add_submenu_page('edit.php?post_type=animal', __('Adoptor List | Animal Admit Form v1.0','jkrcat'), __('Adoptor List','jkrcat'), 'manage_options', 'jkrcat_adoptor_list', array($this, 'jkrcat_adoptor_list_page'));
            add_submenu_page('edit.php?post_type=animal', __('Create Adoptor | Animal Admit Form v1.0','jkrcat'), __('Create Adoptor','jkrcat'), 'manage_options', 'jkrcat_adoptor_add', array($this, 'jkrcat_adoptor_create_page'));
        }

        /* 
        Main options page
        */
        public function jkrcat_options_page(){
            include 'inc/dashboard.php';
        }

        /* 
        Fosteres list page
        */
        public function jkrcat_fosteres_list_page(){
            include 'inc/fostere-list.php';
        }
        

        /* 
        Fostere create page
        */
        public function jkrcat_fostere_create_page(){
            include 'inc/fostere-create.php';
        }

        /* 
        Adoptor list page
        */
        public function jkrcat_adoptor_list_page(){
            include 'inc/adoptor-list.php';
        }

        /* 
        Adoptor create page
        */
        public function jkrcat_adoptor_create_page(){
            include 'inc/adoptor-create.php';
        }

        /*
        Add plugin asset
        */
        function jkrcat_add_assets() {
            if(isset($_GET['page']) && strpos($_GET['page'], "jkrcat_") !== false){
                wp_enqueue_style( 'bootstrap', plugins_url('/assets/css/bootstrap.min.css', __FILE__));
            }                                                                                                   
            wp_register_style( 'jkrcat_style', plugins_url('/assets/css/catcss.css', __FILE__));
            wp_enqueue_style( 'jkrcat_style' );

            if(isset($_GET['page']) && strpos($_GET['page'], "jkrcat_") !== false){
                wp_enqueue_script( 'popper', plugins_url('/assets/js/popper.min.js', __FILE__) , array('jquery') );
                wp_enqueue_script( 'bootstrap', plugins_url('/assets/js/bootstrap.min.js', __FILE__) , array('jquery') );
            } 
            wp_enqueue_script( 'jkrcat_script', plugins_url('/assets/js/catmain.js', __FILE__) , array('jquery') );
	        wp_localize_script( 'jkrcat_script', 'jk_ajax', array('ajax_url' => admin_url( 'admin-ajax.php' ), 'ajax_nonce' => wp_create_nonce('jk_ajax_fun'), 
	        ) );
        }

        /*
        wp default media uploader
        */
        function media_uploader_enqueue() {
            wp_enqueue_media();
            wp_register_script('media-uploader', plugins_url('/assets/js/media-uploader.js' , __FILE__ ), array('jquery'));
            wp_enqueue_script('media-uploader');
        }

        /* 
        Delete Fostere Ajax
        */
        public function ac_delete_fostere_ajax_callback(){
            global $wpdb;
            check_ajax_referer( 'jk_ajax_fun', 'security' );
            $return['msg']= '';
            $return['status'] = true;
        
            $id = $_POST['id'];
        
            if(!current_user_can('administrator')){
                $return['msg'] = 'Sorry you don\'t have permission to delete this';
                $return['status'] = false;
                echo wp_send_json_success($return);
                die();
            }
        
            if($wpdb->delete( $wpdb->prefix.'jkrcat_fostere' , array( 'fostere_id' => $id), array('%d'))){
                $return['msg']= 'Data has been deleted successfully.';
            }else{
                $return['status'] = false;
                $return['msg']= 'Sorry! couldn\'t delete, try again.';
            }
        
        
            echo wp_send_json_success($return);
            die();
        
        }

        /* 
        Create Fostere Ajax
        */
        public function ac_add_fostere_ajax_callback(){
            check_ajax_referer("jk_ajax_fun", "security");
            
            global $wpdb;
            $table = $wpdb->prefix."jkrcat_fostere";

            $return['status'] = true;
            $return['msg'] = '';
            $return['type'] = 'new';
            if(!current_user_can('manage_options')){
                $return['status'] = false;
                $return['msg'] = 'You don\'t have Permission to create Fostere';
                echo wp_send_json_success($return);
                die();
            }

            // all form data 
            $data = $_POST['form_data'];
            foreach($data as $value){
                $dataArray[$value['name']] = $value['value'];
            }

            // check Edit or Add by checking user id
            if(isset($dataArray['user_id']) && $dataArray['user_id']>0){
                $user_id = (int)$dataArray['user_id'];
            }else{
                $user_id = NULL;
            }
            
            if(!$user_id){
                // check if Mobile alredy exist in DB
                $duplicat = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) from $table where mobile = %s", $dataArray['mobile'] ) );
                if($duplicat){
                    $return['status'] = false;
                    $return['msg'] = 'This Mobile ('.$dataArray['mobile'].') already exist in database, please try different one.';
                    echo wp_send_json_success($return);
                    die();
                }
            }

            $table_data = array(
                "name"=> $dataArray['name'],
                "address"=> $dataArray['address'],
                "email"=> $dataArray['email'],
                "mobile"=> $dataArray['mobile'],
                "phone"=> $dataArray['phone'],
                "photo"=> $dataArray['background_image'],
                "create_data"=> current_time('mysql', 1),
                "modify_data"=> current_time('mysql', 1));
            $table_format = array(
                "%s",
                "%s",
                "%s",
                "%s",
                "%s",
                "%d",
                "%s",
                "%s"
            );
            if($user_id){ // if edit

                $i = array_search('create_data', array_keys($table_data));
                unset($table_data["create_data"]);
                unset($table_format[$i]);

                $where = array('fostere_id' => $user_id);
                $status = $wpdb->update($table, $table_data, $where, $table_format, "%d");
            }else{
                $status = $wpdb->insert($table, $table_data, $table_format);

                // if foster add from Cats page 
                $new_fostere_id = $wpdb->insert_id;
                $cats_id = $dataArray['cats_id'];
                if($new_fostere_id && $cats_id){
                    if(update_post_meta($cats_id, 'fosterer', $new_fostere_id)){ // update fost meta.
                        $return['fosterer'] = $new_fostere_id;
                    }else{
                        $return['fosterer'] = false;
                    }
                } // if foster add from Cats page

            }
            if($status){
                if($user_id){ // if edit
                    $return['msg'] = 'Fostere has been <strong>Updated</strong> successfully!';
                    $return['type'] = 'update';
                }else{
                    $return['msg'] = 'Fostere has been <strong>Added</strong> successfully!';
                }
            }else{
                if($user_id){ // if edit
                    $return['msg'] = 'Sorry, couldn\'t update Fostere.';
                }else{
                    $return['msg'] = 'Sorry, couldn\'t create Fostere.';
                }
                $return['status'] = false;
            }
            $return['xxx'] = $status;
            
            echo wp_send_json_success($return);
            die();
        } // Create / update Fostere Ajax


        /**
         * Add existing fosterer to a cat
         */
        public function ac_add_existing_fosterer_cat_ajax_callback(){
            check_ajax_referer("jk_ajax_fun", "security");
            
            global $wpdb;
            $table = $wpdb->prefix."jkrcat_fostere";

            $return['status'] = true;
            $return['msg'] = '';
            if(!current_user_can('manage_options')){
                $return['status'] = false;
                $return['msg'] = 'You don\'t have Permission to chagne setting';
                echo wp_send_json_success($return);
                die();
            }

            $fostere_id = $_POST['fostere_id'];
            $cat_id = $_POST['cat_id'];
            if($fostere_id !== false && $cat_id){
                if(update_post_meta($cat_id, 'fosterer', $fostere_id)){ // update fost meta.
                    $return['status'] = true;
                    $return['msg'] = 'Fosterer added successfully!';
                }else{
                    $return['status'] = false;
                    $return['msg'] = 'Sorry, Please try again.';
                }
            }else{
                $return['status'] = false;
                $return['msg'] = 'Sorry, Please try again.';
            }
            echo wp_send_json_success($return);
            die();
            
        } // add existing fosterer to cat

        /**
         * Remove Fosterer from cat.
         */
        public function ac_delete_foster_from_cat_ajax_callback(){
            check_ajax_referer("jk_ajax_fun", "security");
            
            global $wpdb;
            $table = $wpdb->prefix."jkrcat_fostere";

            $return['status'] = true;
            $return['msg'] = '';
            if(!current_user_can('manage_options')){
                $return['status'] = false;
                $return['msg'] = 'You don\'t have Permission to chagne setting';
                echo wp_send_json_success($return);
                die();
            }

            $fostere_id = $_POST['fostere_id'];
            $cat_id = $_POST['cat_id'];
            if($fostere_id && $cat_id){
                if(update_post_meta($cat_id, 'fosterer', 0)){ // update fost meta.
                    $return['status'] = true;
                    $return['msg'] = 'Fosterer removed successfully!';
                }else{
                    $return['status'] = false;
                    $return['msg'] = 'Sorry, Please try again.';
                }
            }else{
                $return['status'] = false;
                $return['msg'] = 'Sorry, Please try again.';
            }
            echo wp_send_json_success($return);
            die();
            
        } // Remove fosterer from  cat



        /* 
        Show fostere detais on popup
        */
        function ac_view_fostere_ajax_callback(){
            check_ajax_referer("jk_ajax_fun", "security");
            
            global $wpdb;
            $table = $wpdb->prefix."jkrcat_fostere";
            $id = $_POST['id'];

            $return['status'] = true;
            $return['msg'] = '';
            $return['title'] = $id;
            $return['body'] = '';
            if(!current_user_can('manage_options') || !$id){
                $return['status'] = false;
                $return['msg'] = 'You don\'t have Permission to View Fostere defails.';
                echo wp_send_json_success($return);
                die();
            }

            $the_fostere = $wpdb->get_row("SELECT * FROM $table WHERE fostere_id = $id LIMIT 1");
            if($the_fostere){
                $return['title'] = $the_fostere->name;
                $img = wp_get_attachment_image_src($the_fostere->photo, 'medium');
                $animals = $this->ac_count_animal_count_for_fostere($the_fostere->fostere_id);
                $return['body'] = '<div class="row">
                    <div class="col-sm-8">
                        <p><strong>Assigned Animal#</strong>: '.count($animals).'</p>
                        <p><strong>Name</strong>: '.$the_fostere->name.'</p>
                        <p><strong>Email</strong>: '.$the_fostere->email.'</p>
                        <p><strong>Address</strong>: '.$the_fostere->address.'</p>
                        <p><strong>Landline</strong>: '.$the_fostere->phone.'</p>
                        <p><strong>Mobile</strong>: '.$the_fostere->mobile.'</p>
                        <p><strong>Create Data</strong>: '.date('d M, Y | h:m:sA', strtotime($the_fostere->create_data)).'</p>
                        <p><strong>Last Modify</strong>: '.date('d M, Y | h:m:sA', strtotime($the_fostere->modify_data)).'</p>
                    </div>
                    <div class="col-sm-4">
                        <img src="'.$img[0].'" alt="No image found" class="rounded float-left">
                    </div>
                </div>';

                // assigned Cats to this Foster 
                if($animals){
                    $return['body'].= '<hr><div class="list_items">';
                        foreach($animals as $amimal){
                            $return['body'].= '<div class="item row">
                                <div class="col-sm-4">
                                    <label class="red">AARU CODE</label>
                                    <span>'.(get_field('aaru_code',$amimal->ID)? get_field('aaru_code',$amimal->ID) : '<span class="red">NONE</span>').'</span>
                                </div>
                                <div class="col-sm-4">
                                    <label>CAT NAME</label>
                                    <span>'.$amimal->post_title.'</span>
                                </div>
                            
                                <div class="meta col-sm-4 justify-content-end">
                                    <a  href="?post_type=animal&page=jkrcat_setting&cats_id='.$amimal->ID.'"><img src="'.plugin_dir_url( __FILE__ ).'/assets/images/cat-view.png" alt=""> View</a>';
                                    
                                    if(current_user_can('administrator')){
                                        $return['body'].= '<a class="red delete_fostere_cat" href="'.$amimal->ID.'" data-fosterid="'.$id.'">Delete</a> <span class="msg">Wait</span>';
                                    }

                            $return['body'].= '</div>
                                </div>';
                        }
                    $return['body'].= '</div>';
                }else{
                    $return['body'].= '<hr><h5 class="red">No Cats assigned to this Fostere.</h5>';
                } // assignede cats finished 
                  
            }else{
                $return['title'] = 'Unknown!!';
                $return['body']='Sorry!, no data found for this Fostere.';
            }

            echo wp_send_json_success($return);
            die();
        } // view fostere details


        /**
         * get animal count for Foster
        */
        public function ac_count_animal_count_for_fostere($fostere_id){
            $args = array(
                'posts_per_page'   => -1,
                'post_type'     => 'animal',
                'post_status'   => array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit'),
                'meta_query'    => array(
                    array(
                        'key'   => 'fosterer',
                        'value' => $fostere_id,
                        'compare'   => 'LIKE'
                    ))
            );
            $num_animal = get_posts($args);
            return $num_animal;
        }

        /*
         * plugin common footer 
        */
        public function jk_plugin_footer_html(){
            ?>
                <!-- == Modal, view foster details  -->
                <div class="modal" id="foster_details" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Fostere Details</h5>
                        <div class="page_msg"></div>
                        <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </a>
                        
                    
                    </div>
                    <div class="modal-body">
                        <p>Loading...</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <a  class="btn btn-primary" id="edit_fostere" data-href="<?php echo add_query_arg('page', 'jkrcat_fostere_add'); ?>&fos=" href="#">Edit Fostere</a>
                    </div>
                    </div>
                </div>
                </div>
                <!-- == // Modal, view foster details  -->
            <?php
        }

    } // end class
    new jk_robin_hood_cat();
} // end if
?>

