jQuery(document).ready(function($) {
  var mediaUploader;
  $("#upload_image_button, #background_image_show").click(function(e) {
    e.preventDefault();
    $this = $(this);
    if (mediaUploader) {
      mediaUploader.open();
      return;
    }
    mediaUploader = wp.media.frames.file_frame = wp.media({
      title: "Choose Image",
      // frame: "select",
      // state: "insert",

      button: {
        text: "Choose Image"
      },
      library: {
        type: "image" // limits the frame to show only images
      },
      multiple: false
    });
    mediaUploader.on("select", function() {
      var attachment = mediaUploader
        .state()
        .get("selection")
        .first()
        .toJSON();
      $("#background_image").val(attachment.id);
      $("#background_image_url").val(attachment.sizes.medium.url);
      $("#background_image_show").attr("src", attachment.sizes.medium.url);
      $("#upload_image_remove").prop("disabled", false);
    });
    mediaUploader.open();
  });

  // remove image
  $("#upload_image_remove").click(function() {
    console.log("x");
    $("#background_image").val("");
    $("#background_image_url").val("");
    $("#background_image_show").attr(
      "src",
      $("#background_image_show").data("old")
    );
    $(this).prop("disabled", true);
  });

  // on page load
  if ($("#background_image_url").val()) {
    var image_url = $("#background_image_url").val();
    $("#background_image_show").attr("src", image_url);
    $("#upload_image_remove").prop("disabled", false);
  } else {
    $("#upload_image_remove").prop("disabled", true);
    $("#background_image_show").attr(
      "src",
      $("#background_image_show").data("old")
    );
  }
});
