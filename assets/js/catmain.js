var $ = jQuery;
$(document).ready(function() {
  // form submit
  jk_all_form_submit();

  // delete foster
  jkrcat_delete_fostere();

  // create foster
  jkrcat_create_fostere();

  // View fostere details
  jkrcat_view_fostere();

  // Add Existing Fosterer to a Cat
  jkrcat_add_existing_fosterer_to_cat();

  // delete foster from Cat
  jkrcat_delete_foster_from_cat();
}); // doc.ready

function jk_all_form_submit() {
  $(".jkrcat_outer form input[type=submit]").click(function() {
    $(this)
      .closest("body")
      .addClass("formsubmit");
  });
} // jk_all_form_submit

function jkrcat_delete_fostere() {
  $(document).on("click", ".delete_fostere", function(event) {
    event.preventDefault();
    var $this = $(this);
    var $id = $this.attr("href");
    if (!confirm("Are you sure, you want to detete this?")) {
      return false;
    }

    data = {
      action: "ac_delete_fostere",
      id: $id,
      security: jk_ajax.ajax_nonce
    };
    console.log($id);

    jQuery.ajax({
      type: "post",
      dataType: "json",
      url: jk_ajax.ajax_url,
      data: data,
      beforeSend: function() {
        $this
          .hide()
          .next(".msg")
          .show();
        $("body").css({ cursor: "wait" });
      },
      success: function(response) {
        console.log(response);
        if (response.success) {
          if (response.data.status) {
            $this
              .closest(".item")
              .hide(400)
              .remove();
            $(".page_msg").html(
              '<div class="msg_success">Data has been deleted!</div>'
            );
          } else {
            $(".page_msg").html(
              '<div class="msg_error">' + response.data.msg + "</div>"
            );
          }
        } else {
          $(".page_msg").html(
            '<div class="msg_error">Couldn\'t delete, please try again!</div>'
          );
        }
        $("body").css({ cursor: "default" });
        $this
          .show()
          .next(".msg")
          .hide();
      },
      fail: function(e) {
        $(".page_msg").html(
          '<div class="msg_error">Couldn\'t delete, please try again!</div>'
        );
        $("body").css({ cursor: "default" });
        $this
          .show()
          .next(".msg")
          .hide();
      }
    });
  });
}

function jkrcat_create_fostere() {
  // create or update
  $(document).on("submit", "#form_add_fostere", function(event) {
    event.preventDefault();
    var $this = $(this);
    var formData = $this.serializeArray();

    data = {
      action: "ac_add_fostere",
      form_data: formData,
      security: jk_ajax.ajax_nonce
    };

    $.ajax({
      type: "post",
      dataType: "json",
      data: data,
      url: jk_ajax.ajax_url,
      beforeSend: function() {
        $("body").css({ cursor: "wait" });
        $this.find(":input").prop("disabled", true);
      },
      success: function(response) {
        console.log(response);
        if (response.success && response.data.status) {
          $(".page_msg").html(
            '<div class="msg_success">' + response.data.msg + "</div>"
          );
          if (response.data.type == "new") {
            // if edit, no need to clean the form
            $this
              .find(":input")
              .not("input[type=submit]")
              .not("input[type=button]")
              .val("");

            $("#background_image_show").attr(
              "src",
              $("#background_image_show").data("old")
            );
            $("#upload_image_remove").prop("disabled", true);
          } // end if

          //  if cat page and set foster to cat
          if (response.data.fosterer) {
            setTimeout(function() {
              window.location.href = window.location.href;
            }, 1000);
          }

          $("body").removeClass("formsubmit");
        } else {
          $(".page_msg").html(
            '<div class="msg_error">' + response.data.msg + "</div>"
          );
        }

        $("body").css({ cursor: "default" });
        $this.find(":input").prop("disabled", false);
      },
      fail: function(e) {
        $(".page_msg").html(
          '<div class="msg_error">' + response.data.msg + "</div>"
        );

        $("body").css({ cursor: "default" });
        $this.find(":input").prop("disabled", false);
      }
    }); // ajax
  }); // on submit
} // jkrcat create or update fostere

function jkrcat_view_fostere() {
  $(document).on("click", ".view_fostere", function(event) {
    event.preventDefault();
    var $this = $(this);
    var $id = $this.attr("href");

    data = {
      action: "ac_view_fostere",
      id: $id,
      security: jk_ajax.ajax_nonce
    };
    $.ajax({
      type: "post",
      dataType: "json",
      data: data,
      url: jk_ajax.ajax_url,
      beforeSend: function() {
        $("#foster_details").modal("show");
        $("body").css({ cursor: "wait" });
      },
      success: function(response) {
        $("body").css({ cursor: "default" });
        $("#foster_details .modal-title").html(
          "Fostere: <strong>" + response.data.title + "</strong>"
        );
        $("#foster_details .modal-body").html(response.data.body);

        // edit button url
        var empty_edit_url = $("#foster_details #edit_fostere").data("href");
        $("#foster_details #edit_fostere").attr("href", empty_edit_url + $id);
      },
      fail: function(e) {
        $("body").css({ cursor: "default" });
        $("#foster_details .modal-title").html("Not found!");
        $("#foster_details .modal-body").html("Sorry!, Please try again.");
      }
    }); // ajax
  });

  // clear the modal on hide for next time
  $("#foster_details").on("hidden.bs.modal", function(e) {
    if ($(this).hasClass("refrash_page")) {
      // refrash page if needed
      window.location.href = window.location.href;
    }
    $("body").css({ cursor: "default" });
    $(this)
      .find(".modal-title")
      .html("Fostere Details");
    $(this)
      .find(".modal-body")
      .html("Loading...");
  });

  if ($("#foster_details").length && getUrlParameter("fos") != 0) {
    $('.view_fostere[href="' + getUrlParameter("fos") + '"').click();
  }
} // View fostere details

function jkrcat_add_existing_fosterer_to_cat() {
  $("#existing_fosterer").on("change", function(event) {
    event.preventDefault();
    var $this = $(this);
    var ex_fostere = $this.val();
    var cat_id = $this.data("catid");

    data = {
      action: "ac_add_existing_fosterer_cat",
      fostere_id: ex_fostere,
      cat_id: cat_id,
      security: jk_ajax.ajax_nonce
    };

    $.ajax({
      type: "post",
      dataType: "json",
      data: data,
      url: jk_ajax.ajax_url,
      beforeSend: function() {
        $("body").css({ cursor: "wait" });
        $this.prop("disabled", true);
      },
      success: function(response) {
        console.log(response);
        if (response.success && response.data.status) {
          $(".existing_fosterer_msg").html(
            '<div class="msg_success" style="color:green">' +
              response.data.msg +
              "</div>"
          );
          setTimeout(function() {
            window.location.href = window.location.href;
          }, 1000);
        } else {
          $(".existing_fosterer_msg").html(
            '<div class="msg_error">' + response.data.msg + "</div>"
          );
        }

        $("body").css({ cursor: "default" });
        $this.prop("disabled", false);
      },
      fail: function(e) {
        $(".existing_fosterer_msg").html(
          '<div class="msg_error">' + response.data.msg + "</div>"
        );

        $("body").css({ cursor: "default" });
        $this.prop("disabled", false);
      }
    }); // ajax
  });
} // Add Existing Fosterer to a Cat

function jkrcat_delete_foster_from_cat() {
  $(document).on("click", ".delete_fostere_cat", function(event) {
    event.preventDefault();
    var $this = $(this);
    var cat_id = $this.attr("href");
    var fostere_id = $this.data("fosterid");

    if (!confirm("Are you sure, you want to remove Fosterer from Cat?")) {
      return false;
    }

    data = {
      action: "ac_delete_foster_from_cat",
      fostere_id: fostere_id,
      cat_id: cat_id,
      security: jk_ajax.ajax_nonce
    };

    $.ajax({
      type: "post",
      dataType: "json",
      url: jk_ajax.ajax_url,
      data: data,
      beforeSend: function() {
        $this
          .hide()
          .next(".msg")
          .show();
        $("body").css({ cursor: "wait" });
      },
      success: function(response) {
        console.log(response);
        if (response.success) {
          if (response.data.status) {
            if ($this.closest(".modal").length) {
              $this.closest(".modal").addClass("refrash_page");
            } else {
              window.location.href = window.location.href;
            }
            $this
              .closest(".item")
              .hide(400)
              .remove();
            $(".page_msg").html(
              '<div class="msg_success">Fosterer has been removed from the Cat!</div>'
            );
          } else {
            $(".page_msg").html(
              '<div class="msg_error">' + response.data.msg + "</div>"
            );
          }
        } else {
          $(".page_msg").html(
            '<div class="msg_error">Couldn\'t delete, please try again!</div>'
          );
        }
        $("body").css({ cursor: "default" });
        $this
          .show()
          .next(".msg")
          .hide();
      },
      fail: function(e) {
        $(".page_msg").html(
          '<div class="msg_error">Couldn\'t delete, please try again!</div>'
        );
        $("body").css({ cursor: "default" });
        $this
          .show()
          .next(".msg")
          .hide();
      }
    }); // ajax
  });
} // delete foster from Cat

// get parameter from URL
var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
};
